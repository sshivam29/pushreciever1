package com.jio.mhood.poc.push.receiver1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class PushBroadcastReceiver1 extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent arg1) {
		// TODO Auto-generated method stub

		if(arg1.getAction() == "com.jio.mhood.push.intent.RECEIVE"){
			
			Toast.makeText(context, "Push Receiver 1 called", Toast.LENGTH_SHORT).show();
			String pushMesg = arg1.getStringExtra("pushMesg");
			if(pushMesg!=null){
				Toast.makeText(context, "Pushed Message: " + pushMesg, Toast.LENGTH_SHORT).show();	
			}else{
				Toast.makeText(context, "No Pushed Message Received", Toast.LENGTH_SHORT).show();
			}
			
		}
	}

}
